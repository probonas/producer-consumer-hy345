/*
 * GENERALIZED PRODUCER CONSUMER
 *
 * Promponas Giannis csd3522
 */
#include <semaphore.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/types.h>
#include <string.h>
#include <time.h>

//Comment the following to supress producer and consumer output.
#define VERBOSE 
//#define DEBUG

#define N 100
#define PRODUCTION 1000000 
#define SHARED 0

int buf[N];
int buffer_index;

sem_t empty;	  //how many empty cells there are at buf
sem_t with_value; //how many filled cells there are at buf

sem_t wait; //initialized to the number of producers. When it reaches zero
			//all producers have finished producing numbers and 
			//every consumer running should return too.

pthread_mutex_t lock; //locks access to buffer and buffer index.

unsigned int seed; 

struct args {
	int num;
	void* (*f)(void*);
};

void *producer(void* producerID) {

	for (int i = 0; i < PRODUCTION; i++) {

		/*usleep uses microseconds,sleep a random inteval [1,10]*/
		usleep(rand_r(&seed) % 10 + 1);
		/*produce a random number [1,100]*/
		int item = rand() % 100 + 1;

#ifdef VERBOSE
		printf("producer with id %d waiting.\n", *((int*)producerID));
#endif // VERBOSE

		/*if there are no empty cells producer must block,wait for one number to be consumed.*/
		sem_wait(&empty);
		/*acquire lock to access buffer*/
		pthread_mutex_lock(&lock);

		/*update buffer*/
		buf[++buffer_index] = item;

#ifdef VERBOSE
		printf("loop:%d: producer with id %d inserted %d at buffer_index %d.\n", i, *((int*)producerID), item, buffer_index);
#endif // VERBOSE

		/*release lock*/
		pthread_mutex_unlock(&lock);

		/*now,there is one more number to be consumed at buffer_index.consumers can now wake up,if blocked.*/
		sem_post(&with_value);

#ifdef DEBUG

		int i, j;
		sem_getvalue(&empty, &i);
		sem_getvalue(&with_value, &j);

		printf("producer finished.empty:%d,with value:%d.\n", i, j);
#endif // DEBUG
	}

	/*producer produced PRODUCTION numbers, and now there is one less running.*/
	sem_wait(&wait);
	return NULL;
}

void *consumer(void* consumerID) {
	int consumed = 0; //total numbers consumer consumed

	struct timespec timeout; //timeout for sem_timedwait()

	while(1) {

		int there_are_producers;
		/*if semaphore wait has value zero then ALL producers have finished 
		their work and consumer must return.*/
		sem_getvalue(&wait, &there_are_producers);
		
		if (!there_are_producers)
			break;

#ifdef VERBOSE
		printf("consumer with id %d is waiting.\n", *((int*)consumerID));
#endif // VERBOSE

		clock_gettime(CLOCK_REALTIME, &timeout);
		timeout.tv_sec += 1; //set timeout to one second ahead from now.

		/*if there are values to be consumed the following will not block.
		  if it blocks then either the producer(s) aren't producing numbers 
		  fast enough and consumer has to wait,or all producers have returned 
		  and consumer must return as well after.
		  but in any case the consumer will not block indefinitely leading to
		  a deadlock but only for 1 sec.
		  */
		if (sem_timedwait(&with_value, &timeout))
			continue;

		/*acquire lock to access buffer*/
		pthread_mutex_lock(&lock);

		int pos = buffer_index--;
		/*get value*/
		int read_val = buf[pos];
		
		/*release lock*/
		pthread_mutex_unlock(&lock);
		
		/*now that consumer has consumed one,there is one more empty cell*/
		sem_post(&empty);
		
		/*do something with the value read from buffer*/
#ifdef VERBOSE
		printf("consumer with id %d consumed %d from pos %d.\n", *((int*)consumerID), read_val, pos);
#endif // VERBOSE
		consumed++;

#ifdef DEBUG

		int i, j;
		sem_getvalue(&empty, &i);
		sem_getvalue(&with_value, &j);

		printf("consumer finished.empty:%d,with value:%d.\n", i, j);
#endif // DEBUG
	}

	printf("Consumer %d:%d\n", *((int*)consumerID), consumed);

	return NULL;
}

void* run_threads(void* args) {
	struct args * t_args = (struct args*)args;
	pthread_t pid[t_args->num];

	for (int i = 0; i < t_args->num; i++) {
		pthread_create(&pid[i], NULL, t_args->f, &i);
	}

	/*wait for threads to return.*/
	for (int i = 0; i < t_args->num; i++) {
		pthread_join(pid[i], NULL);
	}

	return NULL;
}


void usage(void) {
	puts("Usage ./cp -c number-of-consumers -p -number-of-producers\ne.g. ./cp -c 10 -p 50");
}

int main(int argc, char** argv) {
	seed = time(NULL);
	srand(seed);

	sem_init(&empty, SHARED, N);
	sem_init(&with_value, SHARED, 0);
	
	pthread_mutex_init(&lock, NULL);

	buffer_index = -1;
	int num_prod, num_cons;

	if (argc != 5) {
		usage();
		return 0;
	}

	if (strcmp(argv[1], "-p") == 0 && strcmp(argv[3], "-c") == 0) {
		num_prod = atoi(argv[2]);
		num_cons = atoi(argv[4]);
	}
	else if(strcmp(argv[1], "-c") == 0 && strcmp(argv[3], "-p") == 0) {
		num_cons = atoi(argv[2]);
		num_prod = atoi(argv[4]);
	}
	else {
		usage();
		return 0;
	}

	printf("Running with %d producer(s) and %d consumer(s).\n", num_prod, num_cons);
	sem_init(&wait, SHARED, num_prod);

	struct args* producer_args = (struct args*)malloc(sizeof(struct args));
	struct args* consumer_args = (struct args*)malloc(sizeof(struct args));

	producer_args->num = num_prod;
	producer_args->f = &producer;

	consumer_args->num = num_cons;
	consumer_args->f = &consumer;

	pthread_t pid[2];

	pthread_create(&pid[0], NULL, &run_threads, (void*)producer_args);
	pthread_create(&pid[1], NULL, &run_threads, (void*)consumer_args);
	
	
	pthread_join(pid[0],NULL);
	pthread_join(pid[1],NULL);

	free(producer_args);
	free(consumer_args);

	printf("All threads returned!\n");
	return 0;
}